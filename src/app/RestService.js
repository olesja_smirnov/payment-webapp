import {RestClient} from './core/rest';


export const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    });

    const defaults = { headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            }),
        );
};

export async function loadCustomers() {
    return RestClient.get('/customer');
}

export async function loadCustomerById(id) {
    return RestClient.get(`/customer/${id}`);
}

export async function updateCustomer(customer) {
    return RestClient.put(`/customer/${customer.id}`, customer);
}

export async function searchCustomer(searchParam) {
    return RestClient.post(`/customer/search?searchParam=${searchParam}`);
}