import {Component} from 'react';
import CustomersList from './CustomersList';
import {loadCustomers} from './RestService';

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = { customers: [] };
    }

    componentDidMount() {
        this.loadCustomersFromServer();
    }

    loadCustomersFromServer() {
        loadCustomers()
            .then((customers) =>
                this.setState({ customers })
            )
            .catch(error => {
                console.log(error);
            });
    }

    render() {

        return (
            <CustomersList customers={this.state.customers} />
        );

    }
}

export default MainPage;