import {Component} from 'react';
import EditCustomer from './EditCustomer';

class Customer extends Component {

    render() {

        return (
            <tr>
                <td>{this.props.customer.firstName}</td>
                <td>{this.props.customer.lastName}</td>
                <td><EditCustomer customer={this.props.customer} /></td>
            </tr>
        );
    }
}

export default Customer;