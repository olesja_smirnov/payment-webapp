//@flow
import { RestClient } from './core/rest/RestClient';

export type Config = {
    apiUrl?: string
};

export async function fetchConfig(): Promise<Config> {
    return RestClient.get('/params/global.params.json');
}