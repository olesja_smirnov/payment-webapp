import {Component} from 'react';
import Customer from './Customer';
import {Table} from 'react-bootstrap';
import './CustomersList.scss';

class CustomersList extends Component {

    renderTableData() {
        return this.props.customers.map((customer) => {
            const { id, firstName, lastName, personalCode } = customer
            return (
                <tr key={id}>
                    <td>{firstName}</td>
                    <td>{lastName}</td>
                    <td>{personalCode}</td>
                </tr>
            )
        })
    }

    render() {

        this.props.customers.map(customer =>
            (<Customer
                key={customer.id}
                customer={customer}
            />));

        return (
            <Table striped bordered hover>
                <tbody>
                    <tr className='table-header'>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Personal code</td>
                    </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
        );
    }
}

export default CustomersList;