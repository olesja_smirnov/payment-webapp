//@flow
import { stringify } from 'query-string';
import axios from 'axios';
import Api from './Api';
import type {Try} from '../Try';

export function del<T>(url: string): Promise<Try<T>> {
    return axios.delete(Api.getDefaultPathDescription() + url)
        .then(res => res.data)
        .catch(err => err);
}

export function get<T>(url: string, data?: mixed): Promise<Try<T>> {
    return axios.get(Api.getDefaultPathDescription() + url + (data ? '?' + stringify(data) : ''))
        .then(res => res.data)
        .catch(err => err);
}

export function post<T>(url: string, data?: mixed): Promise<Try<T>> {
    return axios.post(Api.getDefaultPathDescription() + url, data)
        .then(res => res.data)
        .catch(err => err);
}

export function put<T>(url: string, data?: mixed): Promise<Try<T>> {
    return axios.put(Api.getDefaultPathDescription() + url, data)
        .then(res => res.data)
        .catch(err => err);
}

export function patch<T>(url: string, data?: mixed): Promise<Try<T>> {
    return axios.patch(Api.getDefaultPathDescription() + url, data)
        .then(res => res.data)
        .catch(err => err);
}

export const RestClient = {
    del,
    get,
    post,
    put,
    patch
};

export default RestClient;