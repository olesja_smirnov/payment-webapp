//@flow

class Api {

    static defaultPathDescription: string = '';

    static setDefaultPathDescription(pathDescription: string): void {
        Api.defaultPathDescription = pathDescription;
    }

    static getDefaultPathDescription(): string {
        return Api.defaultPathDescription;
    }

}

export default Api;