import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Api from './app/core/rest/Api';
import App from './App';
import type {Config} from "./app/config";
import {fetchConfig} from "./app/config";
import 'babel-polyfill';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './custom.scss';

(async () => {
const config: Config = await fetchConfig();
console.log('Config', config);
Api.setDefaultPathDescription(config.apiUrl || '');

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
})();

reportWebVitals();
